PROJECT_NAME := mao
AUTHOR := Andrej Gelenberg <andrej.gelenberg@udo.edu>
 
CBINS := lid mao_freq mao_temp mao_bat mao_ac mao_cpu
BINS := $(CBINS)

INSTALL_DIRS += lib/udev/rules.d

all: $(BINS)

bat_mon_OBJ  := getint
mao_temp_OBJ := getint
mao_bat_OBJ  := getint
mao_bat:     LIBS = -lm
mao_freq_OBJ := getint
mao_ac_OBJ   := getint
mao_bat_OBJ  := getf str getint gl
lid_OBJ      := lid_stat
achook_OBJ   := lid_stat getint

install: $(DESTDIR)/lib/udev/rules.d/75-achook.rules

75-achook.rules: 75-achook.rules.t
	sed 's|BINDIR|$(BINDIR)|g' $< > $@
 
CLEAN += $(CBINS)

