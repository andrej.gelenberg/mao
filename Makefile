all:

include stickbuild/stickbuild.mk

CFLAGS=-O2 -Wall -mtune=native -march=native

STICKBUILD_REPO = https://bitbucket.org/nikel123/stickbuild.git
stickbuild/stickbuild.mk:
	[ -e '${@D}' ] && rm -rf '${@D}' ; \
	if [ -e '../$@' ] ; then \
	  if [ -e '../${@D}/.git' ] ; then \
	    if git clone '../${@D}' '${@D}' ; then \
	      ( [ -e .gitmodules ] && grep -e '^\[submodule "${@D}"\]$$' .gitmodules ) || \
		    ( [ ! -e .git ] || ( git submodule add '$(STICKBUILD_REPO)' || git checkout .gitmodules ) ) ; \
	    else \
		  ln -s '../${@D}' '${@D}' ; \
		fi ; \
	  else \
	    ln -s '../${@D}' '${@D}' ; \
	  fi ; \
	else \
	  if [ -e .git ] ; then \
	    ( [ -e .gitmodules ] && grep -e '^\[submodule "${@D}"\]$$' .gitmodules ) || \
	      git submodule add '$(STICKBUILD_REPO)' '${@D}' || git checkout .gitmudules ; \
		git submodule update ; \
	  else \
	    git clone '$(STICKBUILD_REPO)' '${@D}' ; \
	  fi ; \
	fi

# vim: tabstop=4 : noexpandtab :
