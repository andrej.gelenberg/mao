/*
Copyright (c) 2011 Andrej Gelenberg <andrej.gelenberg@udo.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

enum stat_fields {
  user_field = 0,
  nice_field,
  system_field,
  idle_field,
  iowait_field,
  irq_field,
  softirq_field,
  steal_field,
  guest_field,
  guest_nice_field,
  last_field
};

enum {
 total = 0,
 idle,
 iowait,
 last_val
};

enum {
  cpu_load = 0,
  io_load
};

typedef unsigned long long int stat_t[last_val];

static char *mao_get_line(FILE *f)
{
  static ssize_t size = 0;
  static char *buf = NULL; /* reuse the buffer to avoid unnecessary  mallocs */
  char *i = buf;
  char *ret = NULL;
  ssize_t c;
  char *t;

  while(1)
  {
    if ( (i - buf + 1) >=  size ) // need to expand the buffer (don't forget '\0' at the end)
    {
       size += 1024;
       if ( size > 1024000 ) goto mao_get_line_error_end;
       t = realloc(buf, size);
       if ( !t ) goto mao_get_line_error_end;
       i = t + (i - buf);
       buf = t;
       *i = '\0';
    }

    c = fread(i, 1, size - 1 - (i - buf), f); // fill buffer (don't forget '\0' at the end)
    if ( c == 0 ) break; // nothing to read anymore
    *(i+c) = '\0';
    t = strchr(i, '\n');
    if ( t ) // found end of line
    {
      *t = '\0';
      break;
    }
    i += c;
  }

  ret = buf;
goto mao_get_line_end;
mao_get_line_error_end:
  ret = NULL;
mao_get_line_end:
  return ret;
}

static int get_stat(stat_t *s)
{
  int ret = 0;
  char *buf = NULL;
  char *bi;
  long long unsigned int stat[last_field];
  long long unsigned int *si;
  int i;
  FILE *f = fopen("/proc/stat", "r");
  if ( ! f ) goto get_stat_error_end;

  buf = mao_get_line(f);
  fclose(f);
  if ( ! buf ) goto get_stat_error_end;

  if ( strncmp("cpu", buf, sizeof("cpu")-1) || (! isspace(buf[3]) ) ) goto get_stat_error_end;
  bi = buf + 4;

  (*s)[total] = 0;
  errno = 0;
  for(i=0, si=stat; i < (sizeof(stat)/sizeof(stat[0])); ++i, ++si)
  {
    if ( *bi == '\0' ) goto get_stat_error_end;
    *si = strtoull(bi, &bi, 0);
    if ( errno ) goto get_stat_error_end;
    if ( *bi && !isspace(*bi) ) goto get_stat_error_end;
    (*s)[total] += *si;
  }

  (*s)[idle] = stat[idle_field];
  (*s)[iowait] = stat[iowait_field];

  goto get_stat_end;
get_stat_error_end:
  ret = -1;
get_stat_end:
  return ret;
}

int main(int argc, char **argv)
{
  int i;
  stat_t s, d, s_old = {0, 0, 0};
  unsigned char v[2], v_old[2] = { 200, 200 };

  while ( !get_stat(&s) )
  {
    for(i=0; i<(sizeof(s)/sizeof(s[0])); ++i)
      d[i] = s[i] - s_old[i];

    for(i=0; i<(sizeof(s)/sizeof(s[0])); ++i)
      s_old[i] = s[i];

    v[cpu_load] = 100 - (100 * (d[idle] + d[iowait])) / d[total];
    v[io_load] = (100 * d[iowait]) / d[total];

    for(i=0; i<(sizeof(v)/sizeof(v[0])); ++i)
    {
      if ( v[i] != v_old[i] )
      {
        printf("c:%3d i:%3d\n", v[cpu_load], v[io_load]);
        for(i=0; i<(sizeof(v)/sizeof(v[0])); ++i)
          v_old[i] = v[i];
        fflush(stdout);
        break;
      }
    }

    sleep(1);   
  }

  return 0;
}
