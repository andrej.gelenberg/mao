/*
Copyright (c) 2011 Andrej Gelenberg <andrej.gelenberg@udo.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#include "getint.h"

int getint(const char *f)
{
  register int fd = open(f, O_RDONLY);
  register int retval = -1;
  char buf[(int)(log(INT_MAX)/log(10)) + 1];
  if ( fd != -1 ) {
    register ssize_t s = read(fd, buf, sizeof(buf)-1);
    if ( s != -1 && s != 0 ) {
      buf[s] = '\0';
      retval = atoi(buf);
    }
  } else {
    fprintf(stderr, "can't read file '%s'\n", f);
  }
  close(fd);
  return retval;
}
