/*
Copyright (c) 2011 Andrej Gelenberg <andrej.gelenberg@udo.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "str.h"

str_t *newstr(size_t len) {
  str_t *ret = malloc(sizeof(*ret) + len + 1);
  if ( ret )
    ret->len = len;
  return ret;
}

str_t *getstrn(const char *str, size_t len) {
  str_t *ret = newstr(len);
  if ( ret ) {
    memcpy(ret->str, str, len);
    ret->str[len] = '\0';
  }
  return ret;
}

str_t *concatstr(const str_t *a, const str_t *b) {
  str_t *ret = NULL;

  if ( (SIZE_MAX - a->len) >= b->len ) {
    ret = newstr(a->len + b->len);
    if ( ret ) {
      memcpy(ret->str, a->str, a->len);
      memcpy(&(ret->str[a->len]), b->str, b->len);
      ret->str[ret->len] = '\0';
    }
  }

  return ret;
}

str_t *getstr(const char *str) {
  return getstrn(str, strlen(str));
}
