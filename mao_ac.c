/*
Copyright (c) 2011 Andrej Gelenberg <andrej.gelenberg@udo.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <glob.h>
#include <stdio.h>

#include "getint.h"

int main(int argc, char **argv) {
  sigset_t m;
  int ac, ac_new, i, ac_old = -2;
  char **pi;
  glob_t g;
  int s = SIGUSR1;

  if ( glob("/sys/class/power_supply/AC*/online", GLOB_NOSORT, NULL, &g) )
    goto main_end;

  sigemptyset(&m);
  sigaddset(&m,SIGUSR1);
  sigaddset(&m,SIGHUP);
  sigaddset(&m,SIGINT);
  sigaddset(&m,SIGQUIT);
  sigaddset(&m,SIGTERM);
  sigprocmask(SIG_BLOCK, &m, NULL);


  while( s == SIGUSR1 ) {
    ac = -1;

    for(i=0, pi=g.gl_pathv; i<g.gl_pathc; ++i, ++pi)
    {
      switch (getint(*pi)) {
        case 0:
          ac = 0;
          break;
        case 1:
          ac = 1;
          goto break_for;
      } 
    }
break_for:

    if ( ac != ac_old )
    {
      printf("%2s\n", ac ? "AC" : "  ");
      ac_old = ac;
      fflush(stdout);
    }

    sigwait(&m, &s);
  }

main_end:
  globfree(&g);
  return 0;
}
