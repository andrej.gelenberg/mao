/*
Copyright (c) 2011 Andrej Gelenberg <andrej.gelenberg@udo.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <glob.h>

#include "getint.h"

int get_max_f()
{
 int ret = -1;
 int new;
 char **pi;
 size_t i;
 glob_t g;

 if ( glob("/sys/devices/system/cpu/cpu*/cpufreq/scaling_cur_freq", GLOB_NOSORT, NULL, &g) )
   goto get_max_f_end;

 for(i=0, pi=g.gl_pathv; i < g.gl_pathc; ++i, ++pi) {
  new = getint(*pi);
  if ( new > ret ) ret = new;
 }
 
get_max_f_end:
 globfree(&g);
 return ret;
}

int main()
{
 float f, old_f = -2;

 while (1) {
  f = get_max_f();
  f /= 1000000.0;

  float d = f - old_f;
  if ( d < 0 )
    d = -d;

  if ( d >= 0.1 ) {
   if ( f < 0 )
    printf("XXX");
   else
    printf("%.1f\n", f);

   old_f = f;

   fflush(stdout);
  }

  sleep(1);
 }

 return 0;
}
