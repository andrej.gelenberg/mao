/*
Copyright (c) 2011 Andrej Gelenberg <andrej.gelenberg@udo.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "getint.h"

int main(int argc, char **argv)
{
  int i, t, nt, old_t=-2;
  char **pi;

  while(1) {
    t = -1;
    for(i=1, pi=&argv[1]; i<argc; ++i, ++pi) {
      nt = getint(*pi);
      if ( nt > t) t = nt;
    }

    if ( t != old_t ) {
      if ( t == -1 )
        printf("XXX\n");
      else
        printf("%3d\n", t/1000);
 
      old_t = t;
      fflush(stdout);
    }
 
    sleep(1);
  }

  return 0;
}
