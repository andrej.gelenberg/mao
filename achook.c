/*
Copyright (c) 2011 Andrej Gelenberg <andrej.gelenberg@udo.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <glob.h>

#include "getint.h"
#include "lid_stat.h"

static void wr_cmd(const char *path, const char *cmd);

extern char **environ;

static const char * const lidpath = "/dev/input/lid";
static const char * const acpath = "/sys/class/power_supply/AC*/online";
static const char * const lnkpwmpolpath = "/sys/class/scsi_host/host*/link_power_management_policy";
static const char * const cpu0govpath = "/sys/devices/system/cpu/cpu*/cpufreq/scaling_governor";
static const char * const hdapspath = "/sys/module/snd_hda_intel/parameters/power_save";
static const char * const dwc = "/proc/sys/vm/dirty_writeback_centisecs";
static const char * const laptop_mode = "/proc/sys/vm/laptop_mode";

static void bsystem(const char *cmd) {
  pid_t pid = fork();
  if ( pid == 0 )
    execl("/bin/sh", "sh", "-c", cmd, NULL);
  else if ( pid == -1 )
    system(cmd);
}

static void _ckwr_cmd(const char *path, const char *cmd) {
  int c;
  int fd;
  char *buf = NULL;
  size_t l;

  l = strlen(cmd);
  if ( ! l ) return;

  fd = open(path, O_RDWR);
  if ( fd == -1 ) {
    fprintf(stderr, "can't open %s for read and write: %m\n", path);
    close(fd);
    wr_cmd(path, cmd); // may be only write will work
    return;
  }

  buf = malloc(l + 2);
  if ( ! buf ) // if there is no ram, when at least write the command
    goto ckwr_cmd_write;
 
  c = read(fd, buf, l + 2);
  // size is differ, also we don't need to check it with strcmp
  if ( (c < l) || (c > (l+1)) || ((c == l+1) && ( buf[l] != '\n'))  )
    goto ckwr_cmd_write;

  // value is the same, also don't need to set it.
  if ( !strncmp(cmd, buf, l) ) {
    c = l;
    goto ckwr_cmd_end;
  }

ckwr_cmd_write:
  if ( lseek(fd, 0, SEEK_SET) == (off_t)-1 )
    fprintf(stderr, "can't seek %s\n", path);
  c = write(fd, cmd, strlen(cmd));
  if ( c  == -1 )
    fprintf(stderr, "can't write command %s into %s: %m\n", cmd, path);
ckwr_cmd_end:
  if ( buf ) free(buf);
  close(fd);
}

static void _wr_cmd(const char *path, const char *cmd) {
  int c;
  int fd = open(path, O_WRONLY);

  if ( fd == -1 ) {
    fprintf(stderr, "can't open %s for write: %m\n", path);
    return;
  }
  c = write(fd, cmd, strlen(cmd));
  if ( c  == -1 )
    fprintf(stderr, "can't write command %s into %s: %m\n", cmd, path);

  close(fd);
}

#define path_cmd(cmd_name) static void cmd_name(const char *path, const char *cmd) { \
  glob_t g; \
  int i; \
  char **pi; \
 \
  if ( glob(path, 0, NULL, &g) ) \
    return; \
 \
  for(i=0, pi=g.gl_pathv; i<g.gl_pathc; ++i, ++pi) \
    _ ## cmd_name(*pi, cmd); \
 \
  globfree(&g); \
}

path_cmd(ckwr_cmd)
path_cmd(wr_cmd)

static char pres(const char *path) {
  glob_t g;
  char ret = -1;
  size_t i;
  char **pi;

  if ( glob(path, GLOB_NOSORT, NULL, &g) ) {
    fprintf(stderr, "glob faild\n");
    goto pres_end;
  }

  for(i=0, pi=g.gl_pathv; i<g.gl_pathc; ++i, ++pi) {
    switch (getint(*pi)) {
       case 0:
         ret = 0;
         break;
       case 1:
         ret = 1;
         goto pres_end;
    }
  }

pres_end:
  return ret;
}

static int ac_online() {
  ckwr_cmd(lnkpwmpolpath, "max_performance");
  ckwr_cmd(cpu0govpath, "ondemand");
  ckwr_cmd(dwc, "500");
  ckwr_cmd(hdapspath, "0");
  ckwr_cmd(laptop_mode, "0");
  bsystem("rfkill unblock bluetooth");
  bsystem("iw dev wlan0 set power_save off");

  return 0;
}

static int ac_offline() {
  ckwr_cmd(cpu0govpath, "powersave");
  ckwr_cmd(hdapspath, "1");
  ckwr_cmd(dwc, "1500");
  ckwr_cmd(lnkpwmpolpath, "min_power");
  ckwr_cmd(laptop_mode, "5");
  bsystem("rfkill block bluetooth");
  bsystem("iw dev wlan0 set power_save on");

  return 0;
}

int main() {
  int ret;
  bsystem("killall -USR1 mao_ac");
  if (pres(acpath))
    ret = ac_online();
  else
    ret = ac_offline();
  return ret;
}
