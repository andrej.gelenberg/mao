#include <glob.h>
#include <stdlib.h>

#include "gl.h"

str_t *glob_find(const str_t *gl, glob_find_f *findf, void *data) {
  __label__ end;

  str_t *ret = NULL;
  glob_t g;
  size_t c;
  char **v;

  if ( glob(gl->str,
            GLOB_MARK|GLOB_NOSORT|GLOB_PERIOD|GLOB_BRACE|GLOB_TILDE_CHECK,
            NULL, &g) )
    goto end;

  for(c = g.gl_pathc, v = g.gl_pathv;
      c;
      --c, ++v) {

    ret = getstr(*v);
    if ( ret && findf(ret, data) )
      goto end;

    free(ret);
  }

  ret = NULL;
end:
  globfree(&g);
  return ret;
}

static int find_true(const str_t *str, void *data) {
  return 1;
}

str_t *glob_find_first(const str_t *gl) {
  return glob_find(gl, find_true, NULL);
}
