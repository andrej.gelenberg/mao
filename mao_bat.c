/*
Copyright (c) 2011 Andrej Gelenberg <andrej.gelenberg@udo.edu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <glob.h>
#include <alloca.h>
#include <string.h>
#include <math.h>
#include <stdarg.h>

#include "getf.h"
#include "getint.h"
#include "str.h"
#include "gl.h"

static const str_t bat_glob = statstr("/sys/class/power_supply/BAT*");

static char verbose = 0;

static void v(const char * const format, ...)
  __attribute__((format(printf,1,2))) __attribute__((unused));

static void v(const char *format, ...) {
  if ( verbose ) {
    va_list ap;
    va_start(ap, format);
    vprintf(format, ap);
    fflush(stdout);
    va_end(ap);
  }
}

static int is_present(const str_t *path) {
  int ret = 0;
  str_t *pp;
  static const str_t present = statstr("/present");

  pp = concatstr(path, &present);
  if ( pp ) {
    ret = getint(pp->str);
    free(pp);
  }

  return ret;
}

static void print_loop(str_t *path) {
  __label__ error_end, end;

  static const str_t full_str = statstr("/{charge,energy}_full");
  static const str_t now_str  = statstr("/{charge,energy}_now");

  str_t *p;
  str_t *full_path = NULL;
  str_t *now_path = NULL;

  float full, now, new;
  float old = -1;

  p = concatstr(path, &full_str);
  if ( !p ) goto end;
  full_path = glob_find_first(p);
  free(p);
  if ( !full_path ) goto end;
  p = concatstr(path, &now_str);
  if ( !p ) goto full_error_end;
  now_path = glob_find_first(p);
  free(p);
  if ( !now_path ) goto full_error_end;

  while(1) {
    full = getf(full_path->str);
    if ( full == NAN )
      goto error_end;

    now = getf(now_path->str);
    if ( now == NAN )
      goto error_end;

    new = round(100 * now / full);
    if ( !(abs(new - old) < 1.0) ) {
      printf("%3.0f\n", new);
      fflush(stdout);
    }
    old = new;

    sleep(60);
  }

error_end:
  free(now_path);
full_error_end:
  free(full_path);
end:
  return;
}

int main(int argc, char **argv) {
  str_t *l;

  if ( (argc > 1) && ( ! strncmp("-v", argv[1], sizeof("-v")) ) )
    verbose = 1;

  v("verbose mode on\n");

  while ( (l = glob_find(&bat_glob, (glob_find_f*)is_present, NULL) ) ) {
    print_loop(l);
    sleep(10);
    free(l);
  }

main_end:
  printf("XXX");
  return 0;
}
